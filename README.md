ゲームのunityプロジェクト(このディレクトリをダウンロードしてリストに追加)：TheBookGameUnityProject/TheBookGame

資料集(仕様書・構成書)：Documents

開発を考えていた企画(この企画はそこから無駄なものをそぎ落としたもの):VariableF + R

この企画のプレゼン資料：Presentation
(プレゼンテーション.pptx, 原稿完成.pdf)

Unity上でテキストがたくさん出てくるゲームなのでTextMeshProと追加の自作スクリプトでいろんな表現ができるようにするのが目標です。
また、プログラマ以外の方がシナリオやイベントを追加しやすいようにエディターを作ってそこからイベントを作成できるようにしてます。

初めてのUnity初めてのC#なので独学で学びながらやってます。勉強用と作業用のコメントがまだたくさんあります。

TheBookGameUnityProject/TheBookGame/Assets/Editor/EventInformationEditor.cs
→テキストを自由に編集し、自作の「タグ」をつけたすことで色々できます。TextMeshProのタグ機能も併用できます。

TheBookGameUnityProject/TheBookGame/Assets/Scripts/ScenarioTags.cs
→「タグ」の内容を書くとこです。

TheBookGameUnityProject/TheBookGame/Assets/Scripts/ScenarioBehaviour.cs
→実際のテキストを表示し、タグの処理を呼び出すところです。

TheBookGameUnityProject/TheBookGame/Assets/Scripts/EventImporter.cs
→エディターに書くタグ名なんかがまとめてあります。