﻿//#if UNITY_EDITOR
//using System.IO;
//using System.Linq;
using UnityEngine;
using UnityEditor;

public class EditorWindowSample : EditorWindow
{
    private ScriptableObjectSample _sample;
    private string _assetPath = "none";

    [MenuItem("Editor/Sample")]
    private static void Create()
    {
        GetWindow<EditorWindowSample>("サンプル");
    }

    private void OnGUI()
    {
        Color defaultColor = GUI.backgroundColor;

        //D&Dするエリア
        GUI.backgroundColor = Color.gray;
        Rect dropArea = GUILayoutUtility.GetRect(0, 50, GUILayout.ExpandWidth(true));
        GUIStyle style = EditorStyles.helpBox;
        style.alignment = TextAnchor.MiddleCenter;
        GUI.Box(dropArea, "Drag & Drop", style);
        //マウスの位置がエリア内
        if (dropArea.Contains(Event.current.mousePosition))
        {
            //現在のイベントを取得
            var eventType = Event.current.type;
            //D&Dで操作更新された時か実行したとき
            if (eventType == EventType.DragUpdated || eventType == EventType.DragPerform)
            {
                //カーソルに+のアイコンを表示
                DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                
                //D&Dの実行
                if (eventType == EventType.DragPerform)
                {
                    //ドラッグを受け付ける(ドラッグしてカーソルにくっ付いてたオブジェクトが戻らなくなるので)
                    DragAndDrop.AcceptDrag();
                    //イベントを使用済みにする
                    Event.current.Use();
                    
                    //ドラッグされたオブジェクト達
                    Object[] obj = DragAndDrop.objectReferences;
                    //ドラッグされたパス
                    string[] path = DragAndDrop.paths;
                    
                    if(obj.Length == 1 && path.Length == 1)
                    {
                        _sample = obj[0] as ScriptableObjectSample;
                        _assetPath = path[0];
                    }
                }
            }
        }

        if(_sample != null)
        {
            using (new GUILayout.VerticalScope(EditorStyles.helpBox))
            {
                GUI.backgroundColor = Color.gray;
                using (new GUILayout.HorizontalScope(EditorStyles.toolbar))
                {
                    GUILayout.Label("設定");
                }
                GUI.backgroundColor = defaultColor;

                _sample.SampleIntValue = EditorGUILayout.IntField("サンプル", _sample.SampleIntValue);
            }

            using (new GUILayout.HorizontalScope(EditorStyles.helpBox))
            {
                GUI.backgroundColor = Color.gray;
                using (new GUILayout.HorizontalScope(EditorStyles.toolbar))
                {
                    GUILayout.Label("ファイル操作");
                }
                GUI.backgroundColor = defaultColor;

                GUILayout.Label("パス：" + _assetPath);

                using (new GUILayout.HorizontalScope(GUI.skin.box))
                {
                    GUI.backgroundColor = Color.magenta;
                    // 書き込みボタン
                    if (GUILayout.Button("書き込み"))
                    {
                        Export();
                    }
                    GUI.backgroundColor = defaultColor;
                }
            }
        }
    }

    private void Export()
    {
        // 読み込み
        ScriptableObjectSample sample = AssetDatabase.LoadAssetAtPath<ScriptableObjectSample>(_assetPath);
        if (sample == null)
        {
            sample = CreateInstance<ScriptableObjectSample>();
        }

        // コピー
        //sample.Copy(_sample);
        EditorUtility.CopySerialized(_sample, sample);

        // 直接編集できないようにする
        _sample.hideFlags = HideFlags.NotEditable;
        // 更新通知
        EditorUtility.SetDirty(_sample);
        // 保存
        AssetDatabase.SaveAssets();
        // エディタを最新の状態にする
        AssetDatabase.Refresh();
    }
}
//#endif