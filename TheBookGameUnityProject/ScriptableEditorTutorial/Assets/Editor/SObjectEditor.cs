﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

public class SObjectEditorWindow : EditorWindow
{
    //ScriptableObjectSampleの変数
    private SObject sample;

    [MenuItem("Editor/SampleA")]
    private static void Create()
    {
        //生成
        GetWindow<SObjectEditorWindow>("サンプル");
    }


    private void OnGUI()
    {
        if(sample == null)
        {
            sample = CreateInstance<SObject>();
        }
    }
}
#endif