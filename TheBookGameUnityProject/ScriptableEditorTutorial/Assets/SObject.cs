﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SubInfo
{
    public int subInt;
    public string subStr;
    public Vector3 vec3;

    public void Test()
    {
        subInt = 2121;
    }
}

[System.Serializable]
public class BaseInfo
{
    public int baseInt;
    public string baseStr;
    public List<SubInfo> subList;

    public BaseInfo(List<SubInfo> sub)
    {
        subList = sub;
    }
}

[CreateAssetMenu(fileName = "SampleObject", menuName = "Test", order = 0)]
public class SObject : ScriptableObject
{
    [SerializeField]
    public List<BaseInfo> list = new List<BaseInfo>();
}