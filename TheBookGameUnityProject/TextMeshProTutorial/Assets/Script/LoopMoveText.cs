﻿using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LoopMoveText : MonoBehaviour
{
    // 変数の定義
    public float distance;
    public float speed;
    public float charaOffset;

    private TextMeshPro textMeshPro;
    private int printTextCount;

    private List<DECORATIVE_TAG> tagList;

    private GameObject buttonCollider;
    private MeshColliderBehaviour buttonColliderBehaviour;
    private Vector3[] meshPos;

    [SerializeField]
    private Material bottonColor;

    bool isFirstUpdate;

    private void Awake()
    {
        // インスタンス取得
        textMeshPro = gameObject.GetComponent<TextMeshPro>();

        printTextCount = 0;

        meshPos = new Vector3[4];

        isFirstUpdate = true;
    }

    private void Start()
    {
        TextObject textObject = Resources.Load("Objects/SampleText/SampleText") as TextObject;
        tagList = new List<DECORATIVE_TAG>(textObject.tagList);
    }

    void Update()
    {
        //①情報取得-----------------------------------------------
        //ForceMeshUpdate:強制的にテキストのメッシュを再生成する
        textMeshPro.ForceMeshUpdate();

        //var:型推論変数型、C++のautoと違って変数にのみ使える
        //TextInfo:全体の情報が入ったクラス
        var textInfo = textMeshPro.textInfo;
        //表示文字数が0ならUpdateしない
        if (textInfo.characterCount == 0)   return;

        //CopyMeshInfoVertexData:メッシュ情報配列とその頂点データをコピーする
        //TMP_MeshInfo[] cachedMeshInfo = textInfo.CopyMeshInfoVertexData();

        //現在時間取得
        float now = Time.time;

        //②情報更新-----------------------------------------------

        //表示文字数が上限でないなら表示文字数を増やす
        if(printTextCount < textInfo.characterCount) printTextCount++;

        //文字ごとの処理
        for (int i = 0; i < textInfo.characterCount; i++)
        {
            //②-①情報取得-----------------------------------------------------
            //CharacterInfo:1文字単位の情報
            var charInfo = textInfo.characterInfo[i];
            //この文字が透明かどうか
            if (!charInfo.isVisible)
            {
                //スキップ
                continue;
            }
            //インデックス取得
            //この文字のマテリアル情報のインデックス
            int materialIndex = charInfo.materialReferenceIndex;
            //この文字の頂点情報のインデックス
            int vertexIndex = charInfo.vertexIndex;
           
            //更新前の頂点情報(いらなくない…？？←いらなそう)
            //Vector3[] sourceVertices = textInfo.meshInfo[materialIndex].vertices;
            //Vector3[] sourceVertices = cachedMeshInfo[materialIndex].vertices;
            //更新した頂点情報
            Vector3[] vertices = textInfo.meshInfo[materialIndex].vertices;

            //②-①タグ判定-----------------------------------------------------
            for(int j = 0; j < tagList.Count; j++)
            {
                //タグの開始インデックスか
                if(i >= tagList[j].strStart && i <= tagList[j].strEnd)
                {

                    if(tagList[j].tag == DECORATIVE_TAG_TYPE.LOOP_UP_DOWN)
                    {
                        //揺れるアニメーション
                        Vector3 tmpVector = distance * Vector3.up * Mathf.Sin(now * speed + charaOffset * i);
                        vertices[vertexIndex + 0] += tmpVector;
                        vertices[vertexIndex + 1] += tmpVector;
                        vertices[vertexIndex + 2] += tmpVector;
                        vertices[vertexIndex + 3] += tmpVector;
                    }
                    else if(tagList[j].tag == DECORATIVE_TAG_TYPE.BOTTON)
                    {
                        //ボタン
                        if(i == tagList[j].strStart && isFirstUpdate)
                        {
                            meshPos[0] = charInfo.topLeft;
                            meshPos[3] = charInfo.bottomLeft;
                        }

                        if(i == tagList[j].strEnd && isFirstUpdate)
                        {
                            //頂点情報の右上と右下を配列要素の1,2へ代入
                            meshPos[1] = charInfo.topRight;
                            meshPos[2] = charInfo.bottomRight;

                            GameObject meshColliderPrefab = Resources.Load("Prefabs/MeshColliders/MeshColliderPrefab") as GameObject;
                            buttonCollider = Instantiate(meshColliderPrefab);

                            if (buttonCollider != null)
                            {
                                buttonColliderBehaviour = buttonCollider.GetComponent<MeshColliderBehaviour>();

                                if (buttonColliderBehaviour != null)
                                {
                                    buttonColliderBehaviour.meshPosSet(meshPos);
                                }
                                else
                                {
                                    Debug.Log("nullRef:buttonColliderBehaviour");
                                }
                            }
                            else
                            {
                                Debug.Log("nullRef:buttonCollider");
                            }
                        }

                        //MeshColliderBehaviorから衝突しているかの情報を受け取る
                        if(!isFirstUpdate)
                        {
                            if (buttonColliderBehaviour.isColliding)
                            {
                                //頂点の色情報を取得
                                Color32[] verticesColors = textInfo.meshInfo[materialIndex].colors32;
                                //alpha値(透明度)を0にする
                                verticesColors[vertexIndex + 0] = bottonColor.color;
                                verticesColors[vertexIndex + 1] = bottonColor.color;
                                verticesColors[vertexIndex + 2] = bottonColor.color;
                                verticesColors[vertexIndex + 3] = bottonColor.color;
                            }
                        }
                    }
                }
            }


            //②-①タグ処理-----------------------------------------------------
            if(false/*isCaret && isFirstUpdate*/)
            {
                //頂点情報の左上と左下を配列要素の0,3へ代入
                meshPos[0] = charInfo.topLeft;
                meshPos[3] = charInfo.bottomLeft;
            }

            if(false/*isVerticalBar && isFirstUpdate*/)
            {
                //頂点情報の右上と右下を配列要素の1,2へ代入
                meshPos[1] = charInfo.topRight;
                meshPos[2] = charInfo.bottomRight;

                GameObject meshColliderPrefab = Resources.Load("Prefabs/MeshColliders/MeshColliderPrefab") as GameObject;
                buttonCollider = Instantiate(meshColliderPrefab);

                if (buttonCollider != null)
                {
                    buttonColliderBehaviour = buttonCollider.GetComponent<MeshColliderBehaviour>();

                    if (buttonColliderBehaviour != null)
                    {
                        //buttonColliderBehaviour.MeshPos = meshPos;
                    }
                    else
                    {
                        Debug.Log("nullRef:buttonColliderBehaviour");
                    }
                }
                else
                {
                    Debug.Log("nullRef:buttonCollider");
                }
            }

            if(false/*isAtSign*/)
            {
                //distance:揺れる時の最大距離 * Vector3.up:上方向の単位ベクトル = 最大の位置ベクトル
                //now:今の時間 * speed:動く速度 = 今の時間で動くべき量
                //characterOffset:文字毎のディレイ * i:何文字目か = 今の文字数で動くべき量
                //今の時間で動くべき量 + 今の文字数で動くべき量 = 動くべき量
                //Sin(動くべき量) = 動くべき量に応じてSinカーブから値を取り出す = 1～-1
                //最大の位置ベクトル * 1～-1 = 最大の位置ベクトルとその逆ベクトルの間の値
                Vector3 tmpVector = distance * Vector3.up * Mathf.Sin(now * speed + charaOffset * i);
                //移動後の頂点位置を保存
                //vertices[vertexIndex + 0] = sourceVertices[vertexIndex + 0] + tmpVector;
                //vertices[vertexIndex + 1] = sourceVertices[vertexIndex + 1] + tmpVector;
                //vertices[vertexIndex + 2] = sourceVertices[vertexIndex + 2] + tmpVector;
                //vertices[vertexIndex + 3] = sourceVertices[vertexIndex + 3] + tmpVector;
                vertices[vertexIndex + 0] += tmpVector;
                vertices[vertexIndex + 1] += tmpVector;
                vertices[vertexIndex + 2] += tmpVector;
                vertices[vertexIndex + 3] += tmpVector;
            }

            

            if(i >= printTextCount)
            {
                //頂点の色情報を取得
                Color32[] verticesColors = textInfo.meshInfo[materialIndex].colors32;
                //alpha値(透明度)を0にする
                verticesColors[vertexIndex + 0].a = 0;
                verticesColors[vertexIndex + 1].a = 0;
                verticesColors[vertexIndex + 2].a = 0;
                verticesColors[vertexIndex + 3].a = 0;
            }

        }

        //変更した頂点情報の更新
        //メッシュ情報配列の終端までループ
        for (int i = 0; i < textInfo.meshInfo.Length; i++)
        {
            //メッシュ情報に反映
            textInfo.meshInfo[i].mesh.vertices = textInfo.meshInfo[i].vertices;
            //実際のメッシュの形状に反映
            textMeshPro.UpdateGeometry(textInfo.meshInfo[i].mesh, i);
        }

        // 色情報の更新
        textMeshPro.UpdateVertexData(TMP_VertexDataUpdateFlags.Colors32);

        isFirstUpdate = false;
    }
}