﻿using UnityEngine;

public class MeshColliderBehaviour : MonoBehaviour
{
    private Mesh mesh;
    private Vector3[] meshPos;
    [SerializeField]
    private Material meshMat;

    private MeshCollider meshCollider;

    public bool isColliding;

    public void meshPosSet(Vector3[] vectors)
    {
        meshPos = new Vector3[vectors.Length];
        meshPos = vectors.Clone() as Vector3[];
    }

    private void Awake()
    {
        //meshCollider取得(アタッチされているゲームオブジェクトの)
        meshCollider = GetComponent<MeshCollider>();
        //Mesh作成
        mesh = new Mesh();

        isColliding = false;
    }

    //Awakeは生成時に呼ばれるため、meshPosに値がsetされていない
    //生成後の最初のフレームで行われるStartならばset後に作成できる
    private void Start()
    {
        if(mesh != null && meshPos != null && meshCollider != null)
        {
            //△ポリゴンの情報
            int[] triangles = new int[6] { 0, 1, 3, 1, 2, 3 };
            //meshの各パラメータ設定
            mesh.vertices = meshPos;
            mesh.triangles = triangles;
            mesh.RecalculateNormals();

            //meshColliderに値設定
            meshCollider.sharedMesh = mesh;
            meshCollider.transform.GetComponent<MeshFilter>().mesh = mesh;
            meshCollider.transform.GetComponent<MeshRenderer>().sharedMaterial = meshMat;
        }
        else
        {
            Debug.Log("nullRef:mesh || meshPos || meshCollider");
        }
    }

    private void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        isColliding = meshCollider.Raycast(ray, out hit, 100.0f);

        if (isColliding) Debug.Log("Colliding");
    }
}
