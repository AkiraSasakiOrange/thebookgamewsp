﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleTextGenerator : MonoBehaviour
{
    //[SerializeField]
    private GameObject sampleTextPrefab;

    void Start()
    {
        sampleTextPrefab = Resources.Load("Prefabs/SampleTextPrefabs/SampleTextPrefab") as GameObject;
        GameObject text = Instantiate(sampleTextPrefab);
    }
}