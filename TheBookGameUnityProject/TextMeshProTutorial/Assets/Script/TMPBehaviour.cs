﻿using UnityEngine;
//TextMeshProUGUIを使うための情報が
using TMPro;

//クラス:TextMeshProUGUI->Canvusへ書き込むUI
//クラス:TextMeshPro->3DオブジェクトとしてのUI
//RequireComponent:TextMeshProがアタッチされてなければエラー
[RequireComponent(typeof(TextMeshPro))]
public class TMPBehaviour : MonoBehaviour
{
    private TextObject textObject;
    private TextObject textMainObject;
    private TextObject textSubObject;
    private TextMeshPro textMeshPro;

    //切り替え時間
    [SerializeField]
    private float switchingSpan;
    //現Spanで流れた合計時間(秒)
    private float deltaTotal = 0;

    //Awake:Startの前に呼ばれる初期化用の関数(スクリプトが非アクティブでも実行される、アタッチしているオブジェクトが非アクティブなら呼ばれない)
    private void Awake()
    {
        textMainObject = Resources.Load("Objects/SampleText/SampleText") as TextObject;
        textSubObject = Resources.Load("Objects/SampleText/SampleSubText") as TextObject;

        textObject = textMainObject;

        //outキーワード:これがついている引数はメソッド内で値が変更されることを明示的に示している。また条件文の中で書いたりすると引数のスコープがその条件分のスコープのものになる
        //TryGetComponent:引数の型のオブジェクトを取得(テンプレートを使ってvarで型推論みたいなこともできる)戻り値は取得できたかをboolで返す
        //↓
        //TextMshProUGUIのインスタンスを取得する。できなかった場合このクラス(TMPBehavior)のenabledをfalseにする
        //enable:MonoBehaviorの機能、trueなら更新されfalseなら更新されない
        if (!TryGetComponent(out textMeshPro)) this.enabled = false;

        SetCharArray_();
    }

    private void Start()
    {
        DECORATIVE_TAG tag = textObject.tagList[0];
        Debug.Log(tag.tag);
        Debug.Log(tag.strStart);
        Debug.Log(tag.strEnd);
    }

    private void Update()
    {
        //Time.delta:前フレームからここまでに流れた時間(秒)
        this.deltaTotal += Time.deltaTime;
        //一定時間経過したら
        if(this.deltaTotal >= this.switchingSpan)
        {
            //数えた時間リセット
            this.deltaTotal = 0;
            //MainとSubを切り替える
            if(textObject == textMainObject)
            {
                textObject = textSubObject;
                SetCharArray_();
            }
            else if(textObject == textSubObject)
            {
                textObject = textMainObject;
                SetCharArray_();
            }
            //MainもSubも使っていない=不明な値が代入されている=エラー
            else
            {
                Debug.Log("textObjectの値が不明");
            }
        }
    }

    //=>:ラムダ宣言演算子
    //Start{SetCharArray_();}でも動いた,メソッド=>メソッドは関数内容の受け渡しと理解できそう
    //private void Start() => SetCharArray_();

    public void SetCharArray_()
    {
        //メンバがそれぞれ値が代入されたかの確認
        if (textObject != null && textMeshPro != null)
        {
            //textObjectの文字列をセット
            textMeshPro.SetCharArray(textObject.CharArr);
        }
        else
        {
            //エラー
            Debug.Log("null retturn");
        }
    }
}