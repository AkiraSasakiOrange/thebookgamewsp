﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class TMPSample: MonoBehaviour
{
    // 変数の定義
    private TextMeshPro textMeshPro;
    private void Awake()
    {
        // インスタンス取得
        if (!TryGetComponent(out textMeshPro)) this.enabled = false;
    }
    
    void Start()
    {
        // 1.Start関数の処理
    }
    
    void Update()
    {
        //ForceMeshUpdate:強制的にテキストのメッシュを再生成する
        textMeshPro.ForceMeshUpdate();

        //var:型推論変数型、C++のautoと違って変数にのみ使える
        //TextInfo:全体の情報が入ったクラス
        var textInfo = textMeshPro.textInfo;

        //表示文字数が0ならUpdateしない
        if (textInfo.characterCount == 0)   return;

        //CopyMeshInfoVertexData:メッシュ情報配列とその頂点データをコピーする
        TMP_MeshInfo[] cachedMeshInfo = textInfo.CopyMeshInfoVertexData();
        
        // 2.それぞれの文字の事前処理
        //文章の文字数分ループ
        for (int index = 0; index < textInfo.characterCount; index++)
        {
            //CharacterInfo:1文字単位の情報
            var charaInfo = textInfo.characterInfo[index];
            //この文字が透明かどうか
            if (!charaInfo.isVisible)
            {
                //スキップ
                continue;
            }

            //この文字のマテリアル情報のインデックス
            int materialIndex = charaInfo.materialReferenceIndex;
            //この文字の頂点情報のインデックス
            int vertexIndex = charaInfo.vertexIndex;

            Vector3[] sourceVertices = cachedMeshInfo[materialIndex].vertices;

            //materialIndexを使ってこの文字の全頂点情報を取り出す
            Vector3[] vertices = textInfo.meshInfo[materialIndex].vertices;
            //materialIndexを使ってこの文字の全頂点の色情報を取り出す
            Color32[] verticesColors = textInfo.meshInfo[materialIndex].colors32;
            // 3.この文字の処理

        }


        //変更した頂点情報の更新
        //メッシュ情報配列の終端までループ
        for (int i = 0; i < textInfo.meshInfo.Length; i++)
        {
            //メッシュ情報に反映
            textInfo.meshInfo[i].mesh.vertices = textInfo.meshInfo[i].vertices;
            //メッシュの形状に反映
            textMeshPro.UpdateGeometry(textInfo.meshInfo[i].mesh, i);
        }
        // 頂点の色情報の更新
        textMeshPro.UpdateVertexData(TMP_VertexDataUpdateFlags.Colors32);
    }
}