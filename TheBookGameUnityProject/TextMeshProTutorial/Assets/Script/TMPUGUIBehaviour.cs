﻿using UnityEngine;
//TextMeshProUGUIを使うための情報が
using TMPro;

namespace Test.text
{
    //クラス:TextMeshProUGUI->Canvusへ書き込むUI
    //クラス:TextMeshPro->3DオブジェクトとしてのUI
    //TextMeshProUGUIに足りない要素は補完する
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class TMPUGUIBehaviour : MonoBehaviour
    {
        public TextObject textObject;
        private TextMeshProUGUI textMeshPro;

        //Awake:Startの前に呼ばれる初期化用の関数(スクリプトが非アクティブでも実行される、アタッチしているオブジェクトが非アクティブなら呼ばれない)
        private void Awake()
        {
            //outキーワード:これがついている引数はメソッド内で値が変更されることを明示的に示している。また条件文の中で書いたりすると引数のスコープがその条件分のスコープのものになる
            //TryGetComponent:引数の型のオブジェクトを取得(テンプレートを使ってvarで型推論みたいなこともできる)戻り値は取得できたかをboolで返す
            //↓
            //TextMshProUGUIのインスタンスを取得する。できなかった場合このクラス(TMPBehavior)のenabledをfalseにする
            //enable:MonoBehaviorの機能、trueなら更新されfalseなら更新されない
            if (!TryGetComponent(out textMeshPro)) this.enabled = false;
        }

        //=>:ラムダ宣言演算子
        //Start{SetCharArray_();}でも動いた,メソッド=>メソッドは関数内容の受け渡しと理解できそう
        private void Start() => SetCharArray_();

        public void SetCharArray_()
        {
            //メンバがそれぞれ値が代入されたかの確認
            if (textObject != null && textMeshPro != null)
            {
                //textObjectの文字列をセット
                textMeshPro.SetCharArray(textObject.CharArr);
            }
            else
            {
                //エラー
                Debug.Log("null retturn");
            }
        }
    }
}