﻿using System.Collections.Generic;
using UnityEngine;

public enum DECORATIVE_TAG_TYPE
{
    LOOP_UP_DOWN = 0,
    BOTTON,
    INVISIBLE,
    MAX
}

public struct DECORATIVE_TAG
{
    public DECORATIVE_TAG_TYPE tag;
    public int strStart;
    public int strEnd;
}

// AssetMenuで右クリックしたときのCreateで選択できる項目の追加
// fileName:作成時のデフォルトネーム menuName:メニュー表示名,/で木構造にできる order:作成メニューの表示位置
[CreateAssetMenu(fileName = "textObj", menuName = "MyText/text", order = 2)]
public class TextObject : ScriptableObject
{
    const char TAG_TOP_CHAR = '^';      //タグの先頭文字
    const char STR_SANDWICH_CHAR = '|'; //タグの適用範囲を囲む文字
    string[] TAG_STRING;

    //Editor側から変更可能
    //TextAreaを使って編集しやすいテキストボックス
    [SerializeField]
    [TextArea(3,5)]
    private string text;
    //文字列保管用のchar配列
    private char[] charArr;
    public List<DECORATIVE_TAG> tagList;

    TextObject()
    {
        TAG_STRING = new string[(int)DECORATIVE_TAG_TYPE.MAX] 
        {
            "LOOP_UP_DOWN",
            "BOTTON",
            "INVISIBLE",
        };
    }

    //文字配列用のプロパティ(c#でのgetter/setterの便利な実装方法)
    //使用時は変数への直接の代入のような感覚で利用できるが裏では処理が走る
    //重い処理は記述しない
    public char[] CharArr
    {
        //getter
        get
        {
            StrToCharArray();
            ReadTag();
            //char配列を返す
            return this.charArr;
        }
        //setter(private)
        private set
        {
            //valueは代入(=)で参照する値
            //Char配列に値を代入
            this.charArr = value;
        }
    }

    //初期設定のtextをchar配列charArrに格納
    public void StrToCharArray()
    {
        CharArr = text.ToCharArray();
    }

    //タグを読み取ってタグ文字を削除してタグリストを作成
    public void ReadTag()
    {
        tagList = new List<DECORATIVE_TAG>();

        for(int i = 0; i < charArr.Length; i++)
        {
            //先頭文字を発見
            if(charArr[i] == TAG_TOP_CHAR)
            {
                //タグ情報
                string tagString = "";
                int strStart;
                int strEnd;

                //タグの種類を取り出す
                int tagIndex;
                for ( tagIndex = i + 1; tagIndex < charArr.Length && charArr[tagIndex] != STR_SANDWICH_CHAR; tagIndex++)
                {
                    tagString += charArr[tagIndex];
                }
                strStart = tagIndex;

                //タグの適用される文字を囲う範囲を探す
                for ( tagIndex++ ; i < charArr.Length && charArr[tagIndex] != STR_SANDWICH_CHAR; tagIndex++) ;
                strEnd = tagIndex;

                //タグ生成
                for(DECORATIVE_TAG_TYPE j = 0; j < DECORATIVE_TAG_TYPE.MAX; j++)
                {
                    if(tagString == TAG_STRING[(int)j])
                    {
                        DECORATIVE_TAG tag = new DECORATIVE_TAG();
                        tag.tag = j;
                        tag.strStart = strStart;
                        tag.strEnd = strEnd;

                        tagList.Add(tag);
                    }
                }
            }
        }
    }
}