﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

public class EventInformationEditor : EditorWindow
{
    //処理を行うクラス
    private EventInformation eventInfo;
    private string assetPath = "none";
    private Color defaultColor;

    Vector2 scrollPosition = new Vector2(0, 0);

    [MenuItem("Editor/EventInfo")]
    private static void Create()
    {
        GetWindow<EventInformationEditor>("EventInfomationEditor");
    }

    public void OnGUI()
    {

        defaultColor = GUI.backgroundColor;

        //D&Dで取り込む
        ImportDragAndDrop();

        //読み込み済み
        if (eventInfo != null)
        {
            //イベントスプライト

            //ファイルパス
            using (new GUILayout.VerticalScope(EditorStyles.helpBox))
            {
                GUILayout.Label("ファイルパス", EditorStyles.boldLabel);
                GUILayout.Label(assetPath);
            }
            //イベントテキスト
            using (new GUILayout.VerticalScope(EditorStyles.helpBox))
            {
                GUILayout.Label("イベントテキスト", EditorStyles.boldLabel);
                GUIStyle style = EditorStyles.textArea;
                style.fixedHeight = 350;
                style.clipping = TextClipping.Clip;
                eventInfo.text = EditorGUILayout.TextArea(eventInfo.text, style);
            }
            //ボタン
            using (new GUILayout.HorizontalScope(EditorStyles.helpBox))
            {
                Color color = new Color(0.95f, 0.8f, 0.8f, 1);
                GUI.backgroundColor = color;
                //タグ生成ボタン
                if (GUILayout.Button("タグ生成"))
                {
                    CreateTagOnEditor();
                }
                color = new Color(0.8f, 0.95f, 0.8f, 1);
                GUI.backgroundColor = color;
                if (GUILayout.Button("データ保存"))
                {
                    SaveData();
                }
                GUI.backgroundColor = defaultColor;
            }

            //タグ生成済み
            if(eventInfo.tagList != null && eventInfo.tagList.Count != 0)
            {
                scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);
                using (new GUILayout.VerticalScope(EditorStyles.helpBox))
                {
                    for(int i = 0; i < eventInfo.tagList.Count; i++)
                    {
                        if(i != 0)
                        {
                            GUILayout.Box("", GUILayout.Width(550), GUILayout.Height(1));
                        }
                        GUILayout.Label("Tag【" + (i + 1) + "】");
                        eventInfo.tagList[i].SettingGUI();
                    }
                }
                EditorGUILayout.EndScrollView();
            }
        }
    }

    private void ImportDragAndDrop()
    {
        //D&Dするエリア
        Rect dropArea = GUILayoutUtility.GetRect(0, 50, GUILayout.ExpandWidth(true));
        GUIStyle style = EditorStyles.helpBox;
        style.alignment = TextAnchor.MiddleCenter;
        GUI.Box(dropArea, "Drag & Drop Area", style);
        //マウスの位置がエリア内
        if (dropArea.Contains(Event.current.mousePosition))
        {
            //現在のイベントを取得
            var eventType = Event.current.type;
            //D&Dで操作更新された時か実行したとき
            if (eventType == EventType.DragUpdated || eventType == EventType.DragPerform)
            {
                //カーソルに+のアイコンを表示
                DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

                //D&Dの実行
                if (eventType == EventType.DragPerform)
                {
                    //ドラッグを受け付ける(ドラッグしてカーソルにくっ付いてたオブジェクトが戻らなくなるので)
                    DragAndDrop.AcceptDrag();
                    //イベントを使用済みにする
                    Event.current.Use();

                    //ドラッグされたオブジェクト達
                    Object[] obj = DragAndDrop.objectReferences;
                    //ドラッグされたパス
                    string[] path = DragAndDrop.paths;

                    if (obj.Length == 1 && path.Length == 1 && obj[0] is EventInformation)
                    {
                        eventInfo = obj[0] as EventInformation;
                        assetPath = path[0];
                    }
                }
            }
        }
    }

    private void CreateTagOnEditor()
    {
        //タグリスト削除
        eventInfo.tagList.Clear();
        //文字列情報を適用
        eventInfo.TextToCharArr();
        //タグ生成
        if (!EventImporter.CreateTag(eventInfo.charArr, eventInfo.tagList, out eventInfo.charArr))
        {
            Debug.Log("ImportTagErr");
            using (new GUILayout.VerticalScope(EditorStyles.popup))
            {
                GUILayout.Label("タグ生成に失敗しました");
            }
            return;
        }
    }

    private void SaveData()
    {
        //文字情報が生成されてないなら生成
        if(eventInfo.charArr == null || eventInfo.charArr.Length == 0)
        {
            eventInfo.TextToCharArr();
        }
        //更新通知
        EditorUtility.SetDirty(eventInfo);
        //保存
        AssetDatabase.SaveAssets();
        //エディタを最新の状態にする
        AssetDatabase.Refresh();
    }
}
#endif