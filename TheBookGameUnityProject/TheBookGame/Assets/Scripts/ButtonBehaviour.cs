﻿using UnityEngine;

//MeshCollider,MeshFilter,MeshRendererのアタッチが必要
[RequireComponent(typeof(MeshCollider))]
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
//ボタンの生成、その動作を行う
public class ButtonBehaviour : MonoBehaviour
{
    //public変数
    public ButtonTag buttonTag;
    public ScenarioBehaviour scenarioBehaviour;

    //private変数
    private Mesh mesh;
    private Vector3[] meshPos;
    [SerializeField]
    private Material meshMat;
    private MeshCollider meshCollider;

    //プロパティ
    public Vector3[] MeshPos
    {
        set { meshPos = value.Clone() as Vector3[]; }
    }

    private void Awake()
    {
        //アタッチされたMeshColliderの取得
        if (!TryGetComponent(out meshCollider))
        {
            enabled = false;
            return;
        }
        mesh = new Mesh();
    }

    //Awakeは生成時に呼ばれるため、meshPos・buttonTagに値がsetされていない
    //生成後の最初のフレームで行われるStartならばset後に作成できる
    private void Start()
    {
        if (mesh != null && meshPos != null && meshCollider != null)
        {
            //三角ポリゴンの頂点イメージ
            //０　　　　　１
            //　┌───┐
            //　│　　／│
            //　│　／　│
            //　│／　　│
            //　└───┘
            //３　　　　　２
            //三角ポリゴンの情報、先頭から３要素ずつ右回りの頂点情報を入れる
            int[] triangles = new int[6] { 0, 1, 3, 1, 2, 3 };
            //meshの各パラメータ設定
            mesh.vertices = meshPos;
            mesh.triangles = triangles;
            mesh.RecalculateNormals();

            //meshColliderに値設定
            meshCollider.sharedMesh = mesh;
            meshCollider.transform.GetComponent<MeshFilter>().mesh = mesh;
            meshCollider.transform.GetComponent<MeshRenderer>().sharedMaterial = meshMat;
        }
        else
        {
            Debug.Log("nullRefErr:mesh || meshPos || meshCollider");
        }
    }

    private void Update()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        //マウスカーソルが重なっている
        if(meshCollider.Raycast(ray, out hit, 100.0f))
        {
            //ボタンごとの挙動
            if(Input.GetMouseButtonDown(0))
            {
                buttonTag.ButtonAction(scenarioBehaviour);
            }
        }
    }

    //メッシュの頂点情報のSetter
    public void meshPosSet(Vector3[] vectors)
    {
        meshPos = new Vector3[vectors.Length];
        meshPos = vectors.Clone() as Vector3[];
    }
}
