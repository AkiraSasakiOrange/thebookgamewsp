﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

//TextMeshProのアタッチが必須
[RequireComponent(typeof(TextMeshPro))]
//イベント情報の入力・保持するクラス
public class EventImporter : MonoBehaviour
{
    //public変数
    //public List<ScenarioTag> tagList;   //タグクラスのリスト、実際に入るのはScenarioTag継承してるクラス
    public EventInformation eventInfo; //イベント情報

    //private変数
    private TextMeshPro textMeshPro;    //アタッチされたTMP

    //定数・readonly
    private static readonly char TMPTAG_TOP_CHAR = '<';         //TMPTagの先頭文字,TMPTAG_END_CHARで閉じられるまでがタグで画面に出力されない
    private static readonly char TMPTAG_END_CHAR = '>';         //TMPTagの終端文字

    private static readonly char TAG_TOP_CHAR = '^';            //Tagの先頭文字,STR_SANDWICH_CHARが来るまでの文字列がTAG_STRINGSになる
    private static readonly char STR_SANDWICH_CHAR = '|';       //Tagの適用範囲文字,TAG_TOP_CHARの後1つ目から2つ目の文字がTagの処理が行われる文字になる
    private static readonly string[] TAG_STRINGS = new string[] //タグの認識用文字列
    {
        "COLOR",
        "LOOP_UP_DOWN",
        "EVENT_BUTTON",
    };
    //これMapのほうがいい
    public static readonly Type[] TAG_TYPES = new Type[]       //認識されたタグに対応されたTagクラス、TAG_STRINGと要素を対応させる
    {
        typeof(ColorTag),
        typeof(LoopUpDownTag),
        typeof(EventButtonTag),
    };

    private void Awake()
    {
        //アタッチされたTMPの取得
        if (!TryGetComponent(out textMeshPro))
        {
            enabled = false;
            return;
        }

        //イベントのロード
        SetEveneToPath("Sample");
    }

    private void Start()
    {
        //イベント画像の生成
        CreateLeftPageImage(eventInfo.leftPageImagePath);
    }

    //文字列からタグを抽出してタグを作成
    //戻り値:タグの作成でエラーがあったか 引数:TMPに設定するchar配列
    public static bool CreateTag(char[] importArr, List<ScenarioTag> tagList, out char[] tmpCharArr)
    {
        //タグのリストが生成されてないなら生成
        if(tagList == null)
        {
            tagList = new List<ScenarioTag>();
        }

        //要素の削除を便利にするためにリスト化
        List<char> charList = new List<char>(importArr);

        //TMPTagの文字数
        int tmpTagStrNum = 0;

        for (int i = 0; i < charList.Count; i++)
        {
            //先頭文字を発見
            if (charList[i] == TAG_TOP_CHAR)
            {
                //タグ情報の格納とタグ文字の削除を同時に行う

                //タグ情報
                string tagString = "";
                bool isTag = false;
                int tagTypeIndex;
                int strStart;
                int strEnd;

                //**Hint**
                //charList.RemoveAt(i):要素を削除するとき削除する次の要素が前の要素の次になる。つまりiは変動してないがcharList[i]は次の要素を参照していることになる

                //先頭文字を削除
                charList.RemoveAt(i);

                //タグの種類認識用の文字列を取り出す
                while (charList[i] != STR_SANDWICH_CHAR)
                {
                    //最終文字まで囲み文字でなかった
                    if (charList.Count == i)
                    {
                        tmpCharArr = null;
                        return false;
                    }
                    tagString += charList[i];
                    //タグ認識文字を削除
                    charList.RemoveAt(i);
                }

                //タグの種類を特定
                for (tagTypeIndex = 0; tagTypeIndex < TAG_STRINGS.Length; tagTypeIndex++)
                {
                    isTag = tagString == TAG_STRINGS[tagTypeIndex];
                    //タグの種類が一致したならその要素数でforを抜ける
                    if (isTag) break;
                }
                if (!isTag)
                {
                    tmpCharArr = null;
                    return false;
                }

                //囲い文字を削除
                charList.RemoveAt(i);
                strStart = i;

                //次の囲い文字が来るまでiを進める
                for (; charList[i] != STR_SANDWICH_CHAR; i++)
                {
                    if (i >= charList.Count)
                    {
                        tmpCharArr = null;
                        return false;
                    }
                }

                //囲い文字を削除
                charList.RemoveAt(i--);
                strEnd = i;

                //タグ生成
                //タグ認識文字列と一致 && 生成するクラスがScenarioTagを継承しているか
                if (isTag && TAG_TYPES[tagTypeIndex].IsSubclassOf(typeof(ScenarioTag)))
                {
                    ScenarioTag tag = (ScenarioTag)Activator.CreateInstance(TAG_TYPES[tagTypeIndex]);
                    if (tag != null)
                    {
                        //値の代入
                        tag.startIndex = strStart - tmpTagStrNum;
                        tag.endIndex = strEnd - tmpTagStrNum;
                        //tagListへ追加
                        tagList.Add(tag);
                        Debug.Log("Add Tag : " + TAG_STRINGS[tagTypeIndex] + " (" + tagTypeIndex + ")");
                    }
                }
            }
            //TMPのタグ発見
            else if (charList[i] == TMPTAG_TOP_CHAR)
            {
                while (charList[i] != TMPTAG_END_CHAR)
                {
                    //タグ内の文字は表示されないのでその文字分進める
                    i++;
                    //タグの文字数をカウント
                    tmpTagStrNum++;
                }
                //TMPTAG_END_CHAR分文字数をインクリメント
                tmpTagStrNum++;
            }
        }
        //エラーなくTag作成が完了,文字列をタグを消したものに更新
        tmpCharArr = charList.ToArray();
        return true;
    }

    public void SetEveneToPath(string resourcesPath)
    {
        //イベントの読み込み
        EventInformation sourceEventInfo = Resources.Load(resourcesPath) as EventInformation;
        eventInfo = Instantiate(sourceEventInfo);
        if (eventInfo == null)
        {
            enabled = false;
            Debug.Log("LoadPathErr:eventInfo");
            return;
        }

        //シナリオ(テキスト)関連
        //TMPにテキストを設定
        textMeshPro.SetCharArray(eventInfo.charArr);

        //イメージ関連
        //Quadを生成して読み込んだイメージをテクスチャに適用

    }

    private void CreateLeftPageImage(string imagePath)
    {
        //ゲームオブジェクト生成
        GameObject leftPageImage = new GameObject("LeftPageImage");
        //Canvas検索
        leftPageImage.transform.parent = GameObject.Find("LeftPageCanvas").transform;
        //アンカーポジションの変更
        leftPageImage.AddComponent<RectTransform>().anchoredPosition = new Vector3(0, 0, 0);
        //縮尺の修正
        leftPageImage.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        //スプライトの追加
        leftPageImage.AddComponent<Image>().sprite = Resources.Load<Sprite>("SampleSprite");
        //アスペクト比は元の画像のものを使う
        leftPageImage.GetComponent<Image>().preserveAspect = true;
        //画像のサイズを元の画像に合わせる
        leftPageImage.GetComponent<Image>().SetNativeSize();
    }
}