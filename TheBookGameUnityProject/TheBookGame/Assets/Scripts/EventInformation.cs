﻿using System;
using System.Collections.Generic;
using UnityEngine;

//イベント情報を持つScriptableオブジェクトを作成する
//アセットメニューで右クリック→TBGMenu/CreateEventInfoで生成可能
[CreateAssetMenu(fileName = "EventInformation", menuName = "TBGMenu/CreateEventInfo", order = 0)]
[Serializable]
public class EventInformation : ScriptableObject
{
    //public変数
    [SerializeField]
    public string   text;      //Unity上で変更される文字
    [SerializeField]
    public char[]   charArr;   //実際に使用されるchar配列
    [SerializeField]
    public bool     needLeftPageImage;  //左面を画像にするか
    [SerializeField]
    public string   leftPageImagePath;  //表示する左面のファイルパス

    [SerializeReference]
    public List<ScenarioTag> tagList = new List<ScenarioTag>();

    public void TextToCharArr()
    {
        if(text != null )
        {
            charArr = text.ToCharArray();
        }
    }
}