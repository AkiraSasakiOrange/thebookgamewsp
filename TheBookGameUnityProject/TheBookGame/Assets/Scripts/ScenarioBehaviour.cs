﻿using System.Collections.Generic;
using UnityEngine;
using TMPro;

//TextMeshProのアタッチが必須
[RequireComponent(typeof(TextMeshPro))]
//TMPのテキストへの処理を文字単位で行う
public class ScenarioBehaviour : MonoBehaviour
{
    //public変数
    public List<GameObject> buttonList = new List<GameObject>();//イベントの持つボタンのオブジェクト

    //private変数
    private TextMeshPro textMeshPro;    //アタッチされたTMP
    private EventImporter eventImporter;//アタッチされたeventImporter
    private int printCharCount;         //表示される文字数

    private void Awake()
    {
        //アタッチされたTMPの取得
        if (!TryGetComponent(out textMeshPro))
        {
            enabled = false;
            return;
        }
        //アタッチされたEventImporterの取得
        if (!TryGetComponent(out eventImporter))
        {
            enabled = false;
            return;
        }
        printCharCount = 0;
    }

    private void Update()
    {
        //TMPのメッシュを再生成
        textMeshPro.ForceMeshUpdate();

        //テキスト情報 = 表示する全体の文字列としての情報
        //文字情報     = 1文字単位の文字情報

        //①テキスト情報取得-----------------------------------------------
        TMP_TextInfo textInfo = textMeshPro.textInfo;
        if (textInfo.characterCount == 0) return;

        //文字情報の構造体をループ外で作成(生成処理を1Fに何度もしたくない)
        CharacterData charData;
        charData.meshInfo = textInfo.meshInfo;
        charData.parent = gameObject.transform;
        //printCharCountが0なら最初のフレーム
        charData.isFirstUpdate = printCharCount == 0;
        printCharCount++;

        for (int i = 0; i < textInfo.characterCount; i++)
        {
            //②文字情報取得-----------------------------------------------
            charData.charInfo = textInfo.characterInfo[i];
            //無効な文字なら処理をスキップ
            if (!charData.charInfo.isVisible) continue;
            //インデックス取得
            charData.materialIndex = charData.charInfo.materialReferenceIndex;
            charData.vertexIndex = charData.charInfo.vertexIndex;
            //頂点位置情報取得
            charData.vertices = textInfo.meshInfo[charData.materialIndex].vertices;

            //文字色を黒に変更
            Color32[] vColors = charData.meshInfo[charData.materialIndex].colors32;

            //                          R  G  B  A
            Color32 color = new Color32(0, 0, 0, 255);

            vColors[charData.vertexIndex + 0] = color;
            vColors[charData.vertexIndex + 1] = color;
            vColors[charData.vertexIndex + 2] = color;
            vColors[charData.vertexIndex + 3] = color;

            //③タグ有効判定-----------------------------------------------
            for (int tagIndex = 0; tagIndex<eventImporter.eventInfo.tagList.Count; tagIndex++)
            {
                //タグの有効インデックスか
                if(i >= eventImporter.eventInfo.tagList[tagIndex].startIndex && i <= eventImporter.eventInfo.tagList[tagIndex].endIndex)
                {
                    charData.now = Time.time;
                    charData.nowIndex = i;
                    charData.scenarioBehaviour = this;
                    //④タグ処理-------------------------------------------
                    eventImporter.eventInfo.tagList[tagIndex].Action(charData);
                }
            }

            //⑤文字表示数処理---------------------------------------------
            if (i >= printCharCount)
            {
                //頂点の色情報を取得
                //Color32[] verticesColors = textInfo.meshInfo[charData.materialIndex].colors32;
                Color32[] verticesColors = textInfo.meshInfo[charData.materialIndex].colors32;
                //alpha値(透明度)を0にする
                verticesColors[charData.vertexIndex + 0].a = 0;
                verticesColors[charData.vertexIndex + 1].a = 0;
                verticesColors[charData.vertexIndex + 2].a = 0;
                verticesColors[charData.vertexIndex + 3].a = 0;
            }
        }

        //⑥情報反映-------------------------------------------------------
        for (int i = 0; i < textInfo.meshInfo.Length; i++)
        {
            //メッシュ情報に反映
            textInfo.meshInfo[i].mesh.vertices = textInfo.meshInfo[i].vertices;
            //メッシュの形状に反映
            textMeshPro.UpdateGeometry(textInfo.meshInfo[i].mesh, i);
        }
        // 色情報の更新
        textMeshPro.UpdateVertexData(TMP_VertexDataUpdateFlags.Colors32);
    }

    public void EventChange(string eventPath)
    {
        //文字表示数を初期化
        printCharCount = 0;

        //各ボタンの削除
        for (int i = 0; i < buttonList.Count; i++)
        {
            Destroy(buttonList[i]);
        }
        buttonList.Clear();

        //イベントの設定
        eventImporter.SetEveneToPath(eventPath);
    }
}

//文字ごとに扱いやすいデータ
public struct CharacterData
{
    public ScenarioBehaviour scenarioBehaviour; //このコンポーネントのアドレス
    public Transform parent;                    //ゲームオブジェクトの位置情報
    public TMP_CharacterInfo charInfo;          //文字情報
    public Vector3[] vertices;                  //文字頂点情報
    public TMP_MeshInfo[] meshInfo;             //メッシュ情報
    public int materialIndex;                   //マテリアルインデックス
    public int vertexIndex;                     //頂点インデックス
    public int nowIndex;                        //文字情報が何番目の文字か
    public float now;                           //開始から経過した時間
    public bool isFirstUpdate;                  //そのイベントの最初のアップデートか
}