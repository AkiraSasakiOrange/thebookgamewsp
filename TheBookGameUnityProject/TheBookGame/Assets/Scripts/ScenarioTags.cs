﻿using System;
using UnityEngine;
using UnityEditor;

//タグの基底クラスと継承クラス
//全タグが継承する抽象クラス
[Serializable]
public abstract class ScenarioTag
{
    //public変数
    public int startIndex;  //char配列のtagが適用される要素数の始め
    public int endIndex;    //char配列のtagが適用される要素数の終わり

    //仮想関数
#if UNITY_EDITOR
    public abstract void SettingGUI();                  //タグの必要なフィールドをEditor上で実装できる
#endif
    public abstract void Action(CharacterData data);    //タグの動作
}

//ボタンを生成するタグが継承する抽象クラス
public abstract class ButtonTag : ScenarioTag
{
    //生成用のメッシュ
    private Vector3[] meshPos;

    public override void Action(CharacterData charData)
    {
        if (charData.isFirstUpdate && charData.nowIndex == startIndex)
        {
            meshPos = new Vector3[4];
            //meshPosのイメージ(■=メッシュ)
            //０→１
            //　■↓
            //３←２
            //頂点情報の左上と左下を配列要素の0,3へ代入
            meshPos[0] = charData.charInfo.topLeft;
            meshPos[3] = charData.charInfo.bottomLeft;
        }
        else if (charData.isFirstUpdate && charData.nowIndex == endIndex)
        {

            //頂点情報の右上と右下を配列要素の1,2へ代入
            meshPos[1] = charData.charInfo.topRight;
            meshPos[2] = charData.charInfo.bottomRight;

            //ボタンのPrefabをロード、インスタンス化
            GameObject buttonPrefab = Resources.Load("Prefabs/ButtonPrefab") as GameObject;
            GameObject button = UnityEngine.Object.Instantiate(buttonPrefab, charData.parent);

            //親のボタンリストに入れる
            charData.scenarioBehaviour.buttonList.Add(button);

            if (button != null)
            {
                ButtonBehaviour buttonBehaviour = button.GetComponent<ButtonBehaviour>();

                if (buttonBehaviour != null)
                {
                    //Startの前に初期化
                    buttonBehaviour.meshPosSet(meshPos);
                    buttonBehaviour.buttonTag = this;
                    buttonBehaviour.scenarioBehaviour = charData.scenarioBehaviour;
                }
                else
                {
                    Debug.Log("nullRefErr:buttonColliderBehaviour");
                }
            }
            else
            {
                Debug.Log("nullRefErr:buttonCollider");
            }
        }
    }

    public abstract void ButtonAction(ScenarioBehaviour scenarioBehaviour);
}

public class ColorTag : ScenarioTag
{
    [SerializeField]
    private Color color;

#if UNITY_EDITOR
    public override void SettingGUI()
    {
        GUILayout.Label("ColorTag", EditorStyles.boldLabel);
        EditorGUILayout.ColorField(color);
    }
#endif

    public override void Action(CharacterData charData)
    {
        color.a = 255;

        Color32[] verticesColors = charData.meshInfo[charData.materialIndex].colors32;

        verticesColors[charData.vertexIndex + 0] = color;
        verticesColors[charData.vertexIndex + 1] = color;
        verticesColors[charData.vertexIndex + 2] = color;
        verticesColors[charData.vertexIndex + 3] = color;
    }
}

//上下にループするアニメーション
public class LoopUpDownTag : ScenarioTag
{
    [SerializeField]
    private float distance = 2.5f;
    [SerializeField]
    private float speed = 2.5f;
    [SerializeField]
    private float charOffset = 2.5f;

#if UNITY_EDITOR
    public override void SettingGUI()
    {
        GUILayout.Label("LoopUpDownTag", EditorStyles.boldLabel);
        using (new GUILayout.HorizontalScope())
        {
            GUILayout.Label("大きさ:");
            distance = GUILayout.HorizontalSlider(distance, 0, 5);
            GUILayout.Label(string.Format("　　　{0:0.0000}", distance));
        }
        using (new GUILayout.HorizontalScope())
        {
            GUILayout.Label("速さ　:");
            speed = GUILayout.HorizontalSlider(speed, 0, 5);
            GUILayout.Label(string.Format("　　　{0:0.0000}", speed));
        }
        using (new GUILayout.HorizontalScope())
        {
            GUILayout.Label("ずれ　:");
            charOffset = GUILayout.HorizontalSlider(charOffset, 0, 5);
            GUILayout.Label(string.Format("　　　{0:0.0000}", charOffset));
        }
    }
#endif

    public override void Action(CharacterData data)
    {
        //揺れるアニメーション
        //distance:揺れる時の最大距離 * Vector3.up:上方向の単位ベクトル = 最大の位置ベクトル
        //now:今の時間 * speed:動く速度 = 今の時間で動くべき量
        //characterOffset:文字毎のディレイ * index:何文字目か = 今の文字数で動くべき量
        //今の時間で動くべき量 + 今の文字数で動くべき量 = 動くべき量
        //Sin(動くべき量) = 動くべき量に応じてSinカーブから値を取り出す = 1～-1
        //最大の位置ベクトル * 1～-1 = 最大の位置ベクトルとその逆ベクトルの間の値
        Vector3 tmpVector = distance * Vector3.up * Mathf.Sin(data.now * speed + charOffset * data.nowIndex);
        data.vertices[data.vertexIndex + 0] += tmpVector;
        data.vertices[data.vertexIndex + 1] += tmpVector;
        data.vertices[data.vertexIndex + 2] += tmpVector;
        data.vertices[data.vertexIndex + 3] += tmpVector;
    }
}

//次のイベント情報に切り替えるボタン
public class EventButtonTag : ButtonTag
{
    [SerializeField]
    private string eventPath = "none";

#if UNITY_EDITOR
    public override void SettingGUI()
    {
        GUILayout.Label("EventButtonTag", EditorStyles.boldLabel);
        GUILayout.Label("イベントパス:" + eventPath);

        using (new GUILayout.HorizontalScope())
        {
            GUILayout.Label("ドラッグ&ドロップ→");
            //D&Dするエリア
            Rect dropArea = GUILayoutUtility.GetRect(300, 30, GUILayout.ExpandWidth(true));
            GUIStyle style = EditorStyles.helpBox;
            style.alignment = TextAnchor.MiddleCenter;
            GUI.Box(dropArea, "Drag & Drop Area", style);
            //マウスの位置がエリア内
            if (dropArea.Contains(Event.current.mousePosition))
            {
                //現在のイベントを取得
                var eventType = Event.current.type;
                //D&Dで操作更新された時か実行したとき
                if (eventType == EventType.DragUpdated || eventType == EventType.DragPerform)
                {
                    //カーソルに+のアイコンを表示
                    DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

                    //D&Dの実行
                    if (eventType == EventType.DragPerform)
                    {
                        //ドラッグを受け付ける(ドラッグしてカーソルにくっ付いてたオブジェクトが戻らなくなるので)
                        DragAndDrop.AcceptDrag();
                        //イベントを使用済みにする
                        Event.current.Use();
                        //ドラッグされたパス
                        string[] paths = DragAndDrop.paths;

                        //パス
                        paths[0] = paths[0].Replace("Assets/Resources/", "");
                        eventPath = paths[0].Replace(".asset", "");
                    }
                }
            }
        }
    }
#endif

    public override void Action(CharacterData charData)
    {
        base.Action(charData);

        //色を置き換える処理
        Color32[] verticesColors = charData.meshInfo[charData.materialIndex].colors32;

        //                          R   G   B   A
        Color32 color = new Color32(100,100,255,255);

        verticesColors[charData.vertexIndex + 0] = color;
        verticesColors[charData.vertexIndex + 1] = color;
        verticesColors[charData.vertexIndex + 2] = color;
        verticesColors[charData.vertexIndex + 3] = color;
    }

    public override void ButtonAction(ScenarioBehaviour scenarioBehaviour)
    {
        //イベントを変更する
        scenarioBehaviour.EventChange(eventPath);
    }
}